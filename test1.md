# Parameter

Für die Bereitstellung der Parameter ist die Erreichbarkeit der Department-Label sicherzustellen.

~~~cpp
std::wstring GetLabel(int const DeptNumber)
{
  return L"Easy";
}
~~~

Diese Funktion ermittelt das der Abteilung zugeordnete Label und gibt es als `wstring` zurück.